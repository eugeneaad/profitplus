package com.citi.profitplus;

import static org.junit.Assert.*;

import org.junit.Test;

import com.citi.profitplus.strategiesLogic.PeriodData;
import com.citi.profitplus.strategiesLogic.StockData;

public class TestStockData {

	@Test
	public void test() {

		StockData testStock = new StockData(127L,"ibm","International Business Machines Corporation");
		// Add some periods to test
		testStock.addPeriod(new PeriodData(0L));
		testStock.addPeriod(new PeriodData(1L));
		testStock.addPeriod(new PeriodData(2L));
		testStock.addPeriod(new PeriodData(3L));
		testStock.addPeriod(new PeriodData(4L));
		testStock.addPeriod(new PeriodData(5L));
		// Test stock and period list
		assertTrue(127 == testStock.getSymbolID());
		assertTrue("ibm".equals(testStock.getSymbol()));
		assertTrue("International Business Machines Corporation".equals(testStock.getCompanyName()));
		assertTrue(6 == testStock.getPeriods().size());
		// Test if the period are added in the right order
		for(int i = 0; i< 6; i++) {
			assertTrue(i == (int)testStock.getPeriods().get(i).getPeriodNumber());
		}
		
	}

}
