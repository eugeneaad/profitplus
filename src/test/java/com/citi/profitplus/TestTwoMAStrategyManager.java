package com.citi.profitplus;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.citi.profitplus.entity.UserInput;
import com.citi.profitplus.strategiesLogic.StockMonitor;
import com.citi.profitplus.strategiesLogic.TwoMAStrategyManager;

public class TestTwoMAStrategyManager {

	@Test
	public void test() {
		
		StockMonitor sm = new StockMonitor();
		TwoMAStrategyManager manager =  new TwoMAStrategyManager(sm);
		// Create dummy user inputs
		List<UserInput> userInputs = new ArrayList<UserInput>();
		List<String> stockSymbols = new ArrayList<String>(Arrays.asList(new String[] { 
			"ibm", "aapl", "msft", "goog"
		}));
		for (String stockSymbol : stockSymbols) {
			UserInput userInput = new UserInput();
			userInput.setStockSymbol(stockSymbol);
			userInputs.add(userInput);
		}
		// For every user input, start a thread
		for (UserInput userInput : userInputs) {
			manager.activate(userInput);
			assertTrue(manager.getActivatedStocks().containsKey(userInput.getStockSymbol()));
		}
		// Cleanup
		for (UserInput userInput : userInputs) {
			manager.forceExit(userInput);
		}
		// Check all removed
		assertTrue(manager.getActivatedStocks().size() == 0);
		
	}

}
