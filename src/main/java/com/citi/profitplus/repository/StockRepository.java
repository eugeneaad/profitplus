package com.citi.profitplus.repository;

import com.citi.profitplus.entity.Stock;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface StockRepository extends CrudRepository<Stock,Long>{

}
