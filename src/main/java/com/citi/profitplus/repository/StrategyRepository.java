package com.citi.profitplus.repository;

import org.springframework.data.repository.CrudRepository;

import com.citi.profitplus.entity.Strategy;

public interface StrategyRepository extends CrudRepository<Strategy,Long>{

}
