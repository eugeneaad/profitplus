package com.citi.profitplus.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="STRATEGIES")
public class Strategy{

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long strategyId;
	private String strategyName;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="strategy_id")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@JsonBackReference
	private Collection<UserInput> userInput;

	public Strategy() {}
	
	public Strategy(long strategyId, String strategyName) {
		this.strategyId = strategyId;
		this.strategyName = strategyName;
	}
	
	public Collection<UserInput> getUserInput() {
		return this.userInput;
	}
	
	public void setUserInput(Collection<UserInput> userInput) {
		this.userInput = userInput;
	}
	
	public long getStrategyId() {
		return this.strategyId;
	}
	
	public String getStrategyName() {
		return this.strategyName;
	}
	
	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}
		
	@Override
	public String toString() {
		return "Strategy [strategyId=" + strategyId + ", strategyName=" + strategyName + "]";
	}
}
