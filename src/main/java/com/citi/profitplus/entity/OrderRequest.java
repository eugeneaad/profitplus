package com.citi.profitplus.entity;

import java.time.LocalDateTime;

//Object class no need create entity
public class OrderRequest {
	private long orderId;
	private boolean buy;
	private double price;
	private int size;
	private String stock;
	private String whenAsDate;     //String literal converted from LocalDateTime
	
	//default constructor 
	public OrderRequest() {}
	
	public OrderRequest(long orderId, boolean buy, double price, int size, String stock) {
		this.orderId = orderId;
		this.buy = buy;
		this.price = price;
		this.size = size;
		this.stock = stock;
		this.whenAsDate = LocalDateTime.now().toString();
	}
	
	public boolean isBuy() {
		return buy;
	}
	public void setBuy(boolean buy) {
		this.buy = buy;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getWhenAsDate() {
		return whenAsDate;
	}
	public void setWhenAsDate(String whenAsDate) {
		this.whenAsDate = whenAsDate;
	}
	
	@Override
	public String toString() {
		return "TradeOrder: {\"buy\":" + buy 
						+ ", \"price\":" + price 
						+ ", \"size\":" + size 
						+ ", \"stock\":" + stock
						+ ", \"whenAsDate\":" + whenAsDate
						+ ", \"order Id\": " + orderId
						+" }";
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

}

