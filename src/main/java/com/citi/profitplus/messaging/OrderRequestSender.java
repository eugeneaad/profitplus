package com.citi.profitplus.messaging;

import java.util.UUID;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.citi.profitplus.entity.OrderRequest;


@Component
public class OrderRequestSender {
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public void sendTradeOrder(OrderRequest orderRequest) {
		System.out.println("Sending Order: " + orderRequest);	
		
		//request queue: OrderBroker
		jmsTemplate.convertAndSend("OrderBroker", orderRequest, m -> {
			System.out.println("Setting standard JMS headers before sending");
			m.setJMSCorrelationID(UUID.randomUUID().toString());
			m.setJMSDestination(new ActiveMQQueue("OrderBroker"));
			return m;
		});  
		
	}

}
