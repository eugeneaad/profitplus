package com.citi.profitplus.messaging;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.citi.profitplus.ProfitplusApplication;
import com.citi.profitplus.entity.Order;
import com.citi.profitplus.entity.OrderReply;
import com.citi.profitplus.entity.UserInput;
import com.citi.profitplus.repository.OrderRepository;
import com.citi.profitplus.strategiesLogic.StrategyImpl;
import com.citi.profitplus.strategiesLogic.TwoMAStrategyManager;

@Component
public class OrderReplySender {

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private StrategyImpl strategyImpl;

	@Autowired
	OrderRepository orderRepo;

	public void sendOrderReply(OrderReply reply, String correID) {
		System.out.println("Sending Reply: " + reply);
		 try {
		 //simulate delay in response within 10 seconds
		 Thread.sleep(new Random().nextInt(10000));
		
		 }catch(Exception ex) {
		 System.out.println("Connection is interrupte before sending reply");
		 }

		updateOrderDB(reply);

		// response queue: OrderBroker_Reply
		jmsTemplate.convertAndSend("OrderBroker_Reply", reply, m -> {
			System.out.println("Setting standard JMS headers before sending REPLY");
			m.setJMSCorrelationID(correID);
			m.setJMSDestination(new ActiveMQQueue("OrderBroker_Reply"));
			return m;
		});

	}

	private void updateOrderDB(OrderReply reply) {
		Order order = orderRepo.findById(reply.getOrderId()).get();
		order.setNumFilled(reply.getNumFilled());
		order.setOrderDate(LocalDateTime.parse(reply.getWhenAsDate()));
		order.setStatus(reply.getResult());
		ProfitplusApplication.dbService.updateOrderStatus(order);
		strategyImpl.receiveOrderReply(order);
		// if(order.getUserInput().getStrategy().getStrategyId() == 1) {
//		if (order.getUserInput().getStrategy().getStrategyId() == 1) {
//			// // TODO Steph strtegy
////			TwoMAStrategyManager ma = (TwoMAStrategyManager)ProfitplusApplication.twoMAManager;
//			try {
//				strategyImpl.receiveOrderReply(order);
////				ma.getActivatedStocks().get(order.getUserInput().getStockSymbol()).receiveOrderReply(order);
//			} catch (Exception ex) {}
//		} else {
//			strategyImpl.receiveOrderReply(order);
//		}
	}
}
