package com.citi.profitplus;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import com.citi.profitplus.entity.*;
import com.citi.profitplus.strategiesLogic.BollingerBands;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UIController {

	@Autowired
	private DatabaseController db;

	// Get Account Balance
	@RequestMapping(method = RequestMethod.GET, value = "/balance", headers = "Accept=application/json, application/xml, text/plain")
	public double getBalance() {
		return db.getBalance();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/strategyhistory", headers = "Accept=application/json, application/xml, text/plain")
	public Iterable<UserInput> getStrategyHistory() {

		Iterable<UserInput> userInputList = db.getStrategyHistory();
		return userInputList;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/allorders", headers = "Accept=application/json, application/xml, text/plain")
	public Iterable<Order> getOrders() {

		Iterable<Order> orders = db.getAllOrder();
		return orders;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/userInput", headers = {
			"Content-Type=application/json, application/xml", "Accept=application/json, application/xml" })
	@ResponseStatus(HttpStatus.CREATED)
	public UserInput insertNewUserInput(@RequestBody LinkedHashMap item) {

		// Check Strategy type
		UserInput userInput = new UserInput();
		double amount = Double.parseDouble(item.get("strategyCapital").toString());
		userInput.setStrategy(db.getStrategy(Long.parseLong(item.get("strategyId").toString())));
		userInput.setStrategyCapital(amount);
		userInput.setStockSymbol(item.get("stockSymbol").toString());
		userInput.setActivate(Boolean.parseBoolean("true"));
		userInput.setUserInputDate(LocalDateTime.now());
		userInput.setAccountBalance(amount);
		
		// If strategy 1
		if (userInput.getStrategy().getStrategyId() == 1) {
			// TODO check strategy activating now
			userInput.setLongPeriod(Integer.parseInt(item.get("longPeriod").toString()));
			userInput.setShortPeriod(Integer.parseInt(item.get("shortPeriod").toString()));
			userInput = db.insertUserInput(userInput);
			// Use the one and only twoMAManager (similar to stockMonitor)
			ProfitplusApplication.twoMAManager.activate(userInput);
		} else if (userInput.getStrategy().getStrategyId() == 2) {
			userInput.setMultiples(Double.parseDouble(item.get("multiples").toString()));
			userInput = db.insertUserInput(userInput);
			// TODO active the second strategy
			try {
				ProfitplusApplication.bbManager.activateStrategy(userInput);
			} catch (InterruptedException e) {
				// // TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(userInput);
		return userInput;
	}

	// Get Orders
	@RequestMapping(method = RequestMethod.GET, value = "/allOrder", headers = "Accept=application/json, application/xml, text/plain")
	public Iterable<Order> getAllOrder() {
		return db.getAllOrder();
	}

	// Update all user input
	@RequestMapping(method = RequestMethod.PUT, value = "/updatestrategy/{id}", headers = {
			"Content-Type=application/json, application/xml", "Accept=*/*" })
	public @ResponseBody void updateUserInput(@PathVariable long id, @RequestBody LinkedHashMap item) {
		System.out.println("Updating User Input ");
		// db.updateUserInput(item);

		UserInput userInput = db.findUserInputById(id);
		double originalCapital = userInput.getStrategyCapital();
		double originalBalance = userInput.getAccountBalance();
		for (Object key : item.keySet()) {
			// System.out.println(key);
			// System.out.println(item.get(key));
			if (key.equals("strategyId"))
				userInput.setStrategy(db.getStrategy(Long.parseLong(item.get(key).toString())));
			else if (key.equals("strategyCapital"))
				userInput.setStrategyCapital(Double.parseDouble(item.get(key).toString()));
			else if (key.equals("stockSymbol"))
				userInput.setStockSymbol(item.get(key).toString());
			else if (key.equals("longPeriod"))
				userInput.setLongPeriod(Integer.parseInt(item.get(key).toString()));
			else if (key.equals("shortPeriod"))
				userInput.setShortPeriod(Integer.parseInt(item.get(key).toString()));
			else if (key.equals("multiples"))
				userInput.setMultiples(Double.parseDouble(item.get(key).toString()));
		}
		double capitalDiff = userInput.getStrategyCapital() - originalCapital;
		double newBalance = originalBalance + capitalDiff;
		if(newBalance > 0) {
			userInput.setAccountBalance(newBalance);
			UserInput newUserInput = db.updateUserInput(userInput);
			
			if(newUserInput.getStrategy().getStrategyId()== 1) {
				//call update in strategy 1
				ProfitplusApplication.twoMAManager.updateUserInput(newUserInput);
			} else {
				//TODO call update in strategy 2
				
			}
			System.out.println("Updated User Input ");
			System.out.println(newUserInput);
		} else {
			//TODO return error
		}

	}

	// Update user input status (activate/deactivate)
	@RequestMapping(method = RequestMethod.PUT, value = "/updatestrategystatus/{id}", headers = {
			"Content-Type=application/json, application/xml", "Accept=*/*" })
	public @ResponseBody void updateUserStatus(@PathVariable int id) throws InterruptedException {
		System.out.println("Updating User Status ");
		UserInput newUserInput = db.updateStatus(id);
		if(newUserInput.getStrategy().getStrategyId() ==1) {
			if(newUserInput.isActivate()) {
				ProfitplusApplication.twoMAManager.activate((newUserInput));
			} else {
				ProfitplusApplication.twoMAManager.deactivate(newUserInput);
			}
			
		} else {
			if(newUserInput.isActivate()) {
				ProfitplusApplication.bbManager.activateStrategy(newUserInput);
			} else {
				ProfitplusApplication.bbManager.deactivateStrategy(newUserInput.getStrategy().getStrategyId());
			}
			
		}
		System.out.println("Updated User Status ");
	}

	// Update user input for strategy 1
	@RequestMapping(method = RequestMethod.PUT, value = "/updatestrategyone/{id}", headers = {
			"Content-Type=application/json, application/xml", "Accept=*/*" })
	public @ResponseBody void updateStrategyOneInput(@PathVariable long id, @RequestBody LinkedHashMap item) {
		System.out.println("Updating Strategy 1 Input ");

		int newShortPeriod = Integer.parseInt(item.get("shortPeriod").toString());
		int newLongPeriod = Integer.parseInt(item.get("longPeriod").toString());
		double newInvAmt = Double.parseDouble(item.get("currentInvestmentAmount").toString());
		UserInput curUserInput = db.findUserInputById(id);
		double curAccBal = curUserInput.getAccountBalance();
		double curInvAmt = curUserInput.getCurrentInvestmentAmount();

		double invAmtDiff = newInvAmt - curInvAmt;
		double newAccBal = curAccBal + invAmtDiff;
		if (newAccBal < 0) {
			// Do not allow change
			throw new AccountBalanceException("New balance will be in negative, unable to change!");

		} else if (newAccBal == 0) {
			// TODO:: if account balance is equivalent to 0
		} else { // approve change

			curUserInput.setAccountBalance(newAccBal);
			curUserInput.setShortPeriod(newShortPeriod);
			curUserInput.setLongPeriod(newLongPeriod);
			curUserInput.setCurrentInvestmentAmount(newInvAmt);
			db.updateUserInput(curUserInput);
		}
		System.out.println("Updated Strategy 1 Input ");
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public class AccountBalanceException extends RuntimeException {
		public AccountBalanceException(String msg) {
			super(msg);
		}
	}

}
