package com.citi.profitplus.strategiesLogic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.List;

public class PeriodData {
	// A period always has 20 price points from the API
	public static final int NUM_PRICES = 20;
	// Attributes
	private String startTime;
	private String endTime;
	private long periodNumber;
	// 20 prices per period
	private List<Double> prices;
	// Highest and lowest for that period
	private double highestPrice;
	private double lowestPrice;
	// Average price for this period
	private double priceAverage;
	
	// Constructor
	public PeriodData(long periodNumber) {
		this.prices = new ArrayList<Double>();
		this.periodNumber = periodNumber;
		this.highestPrice = Double.NEGATIVE_INFINITY;
		this.lowestPrice = Double.POSITIVE_INFINITY;
		this.priceAverage = 0.0;
	}

	// Accessors
	public List<Double> getPrices() { return this.prices; }
	public double getOpeningPrice() { return this.prices.get(0); }
	public double getClosingPrice() { return this.prices.get(this.prices.size() - 1); }
	public double getHighestPrice() { return this.highestPrice; }
	public double getLowestPrice() { return this.lowestPrice; }
	public String getStartTime() { return this.startTime; }
	public String getEndTime() { return this.endTime; }
	public long getPeriodNumber() { return this.periodNumber; }
	public double getPriceAverage() { return this.priceAverage; }
	
	// Mutators
	public void addPrice(double price, String time) {
		// If first price, update start time
		if(this.prices.size()==0) {
			this.startTime = time;
			// Validate that the new price come after the previous one
		} else{
			// Parse both into timing objects (assumed valid), compute difference
			DateFormat df = new SimpleDateFormat("hh:mm:ss");
			try {
				Date prevTime = df.parse(this.endTime);
				Date newTime = df.parse(time);
				// Calculate difference
				long diff = newTime.getTime() - prevTime.getTime();
				// Don't add if its not the next price (1s after)
				if(diff != 1000) return; 
			}catch(ParseException ex) { return; }
		}
		// Update the end time
		this.endTime = time;
		// Add new price
		this.prices.add(price);
		// Check if highest or lowest price
		if(this.highestPrice < price) this.highestPrice = price;
		if(this.lowestPrice > price) this.lowestPrice = price;
		// Rolling update for average
		this.priceAverage = (double)(price +
				(this.priceAverage * (this.prices.size() - 1))) / 
				(double)this.prices.size();
	}
	
}
