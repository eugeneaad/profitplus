package com.citi.profitplus.strategiesLogic;

import java.util.List;

import com.citi.profitplus.ProfitplusApplication;
import com.citi.profitplus.entity.Order;
import com.citi.profitplus.entity.UserInput;
import com.citi.profitplus.messaging.OrderReplyListener;

public class BollingerBands extends StrategyImpl {

	OrderReplyListener replyListener = new OrderReplyListener();

	private double upperLimit;
	private double lowerLimit;
	private double multipliedSd;
	private List<Double> latestFeedPrices;
	private UserInput userInput;
	private boolean activated;
	private BBManager bbManager;
	private boolean forceExit;

	public BollingerBands(UserInput userInput,BBManager bbManager) {
		this.userInput = userInput;
		this.bbManager = bbManager;
	}

	// Activation stuff
	public boolean isActivated() { return this.activated; }

	public void deactivate() { this.activated = false; }
	
	@Override
	public void run() {
		this.activated = true;
		System.out.println("Bollinger Bands is running...");
		while (!this.forceExit) {
			if(this.activated) {
				executeStrategy();
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}

	// Buy or sell based on constant price check
	@Override
	public void executeStrategy() {
		
		userInput = ProfitplusApplication.dbService.findUserInputById(this.userInput.getUserInputId());
		if(!userInput.isActivate() || userInput.getAccountBalance()<0) {
			return;
		}
		priceCheck();
		determineBuyOrSell();
		determineForceExit();
	}
	//
	public void determineBuyOrSell() {
		double sma = SimpleMovingAverage();
		Order newOrder;
		if (sma < lowerLimit && userInput.getAccountBalance()>0 ) {
			// Create order object for mq
			newOrder = createOrderRequest(true, this.userInput);
			
			if (newOrder.getQuantityToBuyOrSell() > 0 && userInput.getAccountBalance()>=0) {
				// send mq request
				SendOrderToMQ(newOrder);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
			else {
				System.out.println("Not enough balance to buy, waiting for time to sell");
			}
			
		} else if (sma > upperLimit && userInput.getCurrentStockQuantity() > 0 ) {
			// Create order object for mq
			newOrder = createOrderRequest(false, this.userInput);
			if (newOrder.getQuantityToBuyOrSell() > 0 && userInput.getAccountBalance()>=0) {
				// send mq request
				SendOrderToMQ(newOrder);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	// Check if we should permanently exit the strategy
	public void determineForceExit() {
		//Printing profit calculation to show current account balance and profit/loss
		double profitPercentage = StrategyImpl.profitCalculation(this.userInput);
		System.out.println("Profit[Loss] (%):" + profitPercentage);
		System.out.println("AB: " + this.userInput.getAccountBalance());
		System.out.println("SC: " + this.userInput.getStrategyCapital());
		// If we made 1% profit or 1% loss
		if (profitPercentage >= 1.0 || profitPercentage <= -1.0) {
			StrategyImpl.forceExit(this.userInput);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			// Ask my manager to remove this thread
			this.bbManager.getActivatedStocks().remove(this.userInput.getUserInputId());
			// Ok can start to kill thread now
			this.forceExit = true;
		}
	}
	
	// Looping to check for market rate
	public void priceCheck() {
		// get latest price from api

		List<Double> latestFeedList = getFeedRestCall(this.userInput.getStockSymbol());
		this.latestFeedPrices = latestFeedList;

		// user defined multiples times sd
		this.multipliedSd = standardDeviation(latestFeedPrices) * this.userInput.getMultiples();

		// update upperLimit of the algo
		this.upperLimit = this.SimpleMovingAverage() + multipliedSd;

		// update lowerLimit of the algo
		this.lowerLimit = this.SimpleMovingAverage() + multipliedSd;
	}

	// standard deviation calculation
	public double standardDeviation(List<Double> feed) {
		double squaredResult = 0;
		double mean = this.SimpleMovingAverage();
		for (int i = 0; i < feed.size(); i++) {
			squaredResult += (feed.get(i) - mean) * (feed.get(i) - mean);
		}
		double meanOfSquaredResults = squaredResult / feed.size();
		return Math.sqrt(meanOfSquaredResults);
	}

	public double SimpleMovingAverage() {
		List<Double> latestFeedPrice = getFeedRestCall(this.userInput.getStockSymbol());
		double sum = latestFeedPrice.stream().mapToDouble(i -> i).sum();
		double average = sum / latestFeedPrice.size();
		return average;
	}

	// allow user to adjust strategy input
	public void adjustStrategy(long userInputStrategyId, double strategyCapital, double multiples) {
		UserInput userInputStrategy = ProfitplusApplication.dbService.findUserInputById(userInputStrategyId);
		userInputStrategy.setStrategyCapital(strategyCapital);
		userInputStrategy.setMultiples(multiples);
		ProfitplusApplication.dbService.updateUserInput(userInputStrategy);
	}
	
	public void receiveOrderReply(Order reply) {      
			if (reply.getNumFilled() == reply.getQuantityToBuyOrSell()) {
				if (reply.isBuyOrSellFlag()) {
					StrategyImpl.Buy(reply);
				} else {
					StrategyImpl.Sell(reply);
				}
			} else if (reply.getNumFilled() < reply.getQuantityToBuyOrSell()) {
				// update trade message after storing
//				Order updatedOrder = createOrderRequest(reply.isBuyOrSellFlag(), reply.getUserInput());
//				SendOrderToMQ(updatedOrder);
//				sendNewOrder.sendTradeOrder(updatedOrder);
//				totalOrderQuantity = newOrder.getSize();
				// recurse to listen for response
//				recursiveOrder(reply, newOrder, totalOrderQuantity, loops - 1);
			}
			System.out.println("Reply:++++++++++++++++++++++++++++++++++++++" + reply);
	}

}
