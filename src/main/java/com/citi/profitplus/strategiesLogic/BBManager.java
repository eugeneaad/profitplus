package com.citi.profitplus.strategiesLogic;

import java.util.HashMap;

import com.citi.profitplus.ProfitplusApplication;
import com.citi.profitplus.entity.UserInput;

public class BBManager {

	// Keep track of threads for each stock
	HashMap<Long, BollingerBands> activatedStocks; //userInput ID and the thread

	public BBManager() {
		this.activatedStocks = new HashMap<Long, BollingerBands>();
	}
	
	public HashMap<Long, BollingerBands> getActivatedStocks(){
		return this.activatedStocks;
	}
	/////////////////////////////////////// Strategy
	/////////////////////////////////////// Activation/////////////////////////////////////////
	// The strategy instance that call this will be able to
	// keep checking for price,
	// run the strategy,
	// check for exit condition
	public void activateStrategy(UserInput userInput) throws InterruptedException {
		BollingerBands newBB = new BollingerBands(userInput, this);

		if (this.activatedStocks.containsKey(userInput.getUserInputId())) {  //&& this.activatedStocks.get(userInput.getUserInputId()).isAlive()){
			this.activatedStocks.get(userInput.getUserInputId()).start();
		} else {
			this.activatedStocks.put(userInput.getUserInputId(), newBB);
			this.activatedStocks.get(userInput.getUserInputId()).start();
		}

	}

	// Force Exit
	public void deactivateStrategy(long userInputStrategyId) {
		UserInput userInputStrategy = ProfitplusApplication.dbService.findUserInputById(userInputStrategyId);
		userInputStrategy.setActivate(false);
		StrategyImpl.forceExit(userInputStrategy);
		ProfitplusApplication.dbService.updateUserInput(userInputStrategy);
		//this.activatedStocks.get(userInputStrategy.getUserInputId()).interrupt();
	}
}



