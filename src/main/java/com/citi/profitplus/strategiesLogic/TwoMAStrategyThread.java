package com.citi.profitplus.strategiesLogic;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.weaver.ast.Or;

import com.citi.profitplus.ProfitplusApplication;
import com.citi.profitplus.entity.Order;
import com.citi.profitplus.entity.UserInput;

public class TwoMAStrategyThread extends Thread {

	// Private attributes
	private String stockSymbol;
	private StockMonitor stockMonitor;
	// short and long MA (defined by number of periods it tracks)
	private int shortMAPeriod, longMAPeriod;
	// Track the last period of both MAs
	private Long lastShortMAPeriodNumber = null, lastLongMAPeriodNumber = null;
	// The MAs themselves
	private List<Double> shortMA, longMA;
	// Whether the strategy is still activated
	private boolean activated;
	private boolean forceExit;
	private boolean waitingForceExit;
	// Can only buy after sell / sell after buy etc.
	//private Boolean isBuying = null;
	private Long lastActivityPeriodNumber = null;
	private UserInput userInput;
	// Manager for this thread
	private TwoMAStrategyManager twoMAManager;
	// Retries for partially filled
	private int partiallyFilledRetries;
	
	// Initialize required parameters
	public TwoMAStrategyThread(UserInput userInput, StockMonitor stockMonitor, TwoMAStrategyManager twoMAManager) {
		this.userInput = userInput;
		this.stockSymbol = userInput.getStockSymbol();
		this.stockMonitor = stockMonitor;
		this.shortMA = new ArrayList<Double>();
		this.longMA = new ArrayList<Double>();
		this.shortMAPeriod = userInput.getShortPeriod();
		this.longMAPeriod = userInput.getLongPeriod();
		this.forceExit = false;
		this.waitingForceExit = false;
		this.twoMAManager = twoMAManager;
		this.partiallyFilledRetries = 0;
		// Assign capital balance / to each other
		this.userInput.setAccountBalance(this.userInput.getStrategyCapital());
	}
	
	// Determine if we should buy or sell based on moving averages
	private void determineBuyOrSell() {
		// If unlucky and periods just reset, wait...
		if (shortMA.size() <= this.shortMAPeriod ||
			longMA.size() <= this.longMAPeriod) return;
		// Values
		// prevShort & prevLong => most recent 
		// prevShort2 & prevLong2 => second most recent
		double prevShort = shortMA.get(shortMA.size() - 1);
		double prevShort2 = shortMA.get(shortMA.size() - 2);
		double prevLong = longMA.get(longMA.size() - 1);
		double prevLong2 = longMA.get(longMA.size() - 2);
		// Make sure the last activity is not in the most recent period
		// Otherwise will be multiple times in same period...
		if (this.lastActivityPeriodNumber == this.lastShortMAPeriodNumber) return;
		// If no issues, lastShortMAPeriod always equals lastLongMAPeriod
		// e.g. lastShortMAPeriod is the last full (20 prices) period at which the
		//      moving average for that period is already calculated
		// Short average exceed long average recently
		Order order;
		// TODO hard code price and quantity
		if (prevShort2 <= prevLong2 && prevShort > prevLong
			/* && (this.isBuying == null || this.isBuying)*/) {
			//order = new Order(this.userInput, true, 100, 100, this.stockSymbol );
			order = StrategyImpl.createOrderRequest(true, this.userInput);
			StrategyImpl.SendOrderToMQ(order);
			this.lastActivityPeriodNumber = this.lastShortMAPeriodNumber;
			//this.isBuying = false;
			System.out.println("2MA is sending order to buy $" + order + " for " + this.stockSymbol);
		} else if (prevShort2 >= prevLong2 && prevShort < prevLong
				   /* && (this.isBuying == null || !this.isBuying)*/) {
			//order = new Order(this.userInput, false, 100, 100, this.stockSymbol );
			order = StrategyImpl.createOrderRequest(false, this.userInput);
			StrategyImpl.SendOrderToMQ(order);
			this.lastActivityPeriodNumber = this.lastLongMAPeriodNumber;
			//this.isBuying = true;
			System.out.println("2MA is sending order to sell $" + order + " for " + this.stockSymbol);
		}
	}
	
	// Update moving averages (depending on whether the period is modified or not)
	public Long updateMovingAverage(List<PeriodData> periods, List<Double> ma, 
									Long lastPeriodNumber, int maPeriod, boolean isPeriodModified) {
		
		// Find index of first new period to the 2MA strategy
		int periodIndex = -1;
		// Find first new period
		if (lastPeriodNumber == null) {
			periodIndex = 0;
		} else {
			// Search from back for the next period
			for (int i = periods.size() - 1; i >= 0; i--) {
				if (periods.get(i).getPeriodNumber() == lastPeriodNumber &&
					i + 1 < periods.size()) {
					periodIndex = i + 1;
					break;
				}
			}
		}
		// periodIndex = index of first new period in the list of PeriodData
		// If there are no new periods, stop
		if (periodIndex == -1) return lastPeriodNumber;
		
		// Start adding into list of moving averages of each period's price avg.
		double rollingAverage = 0.0;
		if (ma.size() == 0) {
			// Add corresponding nulls first (can't compute MA)
			for (int i = 0; i < maPeriod - 1; i++) {
				ma.add(null);
				rollingAverage += periods.get(periodIndex++).getPriceAverage();
			}
			// Add the first moving average
			rollingAverage = (periods.get(periodIndex++).getPriceAverage() + rollingAverage) / maPeriod;
			ma.add(rollingAverage);
		// Otherwise, set rolling average to previous MA
		} else {
			rollingAverage = ma.get(ma.size() - 1);
		}
		// Go through till the last FULL 20s period
		Long newLastPeriodNumber = lastPeriodNumber;
		while(periodIndex < periods.size() && 
			  periods.get(periodIndex).getPrices().size() == PeriodData.NUM_PRICES) {
			// Track last period in MA
			newLastPeriodNumber = periods.get(periodIndex).getPeriodNumber();
			// Move the rolling average along
			rollingAverage *= maPeriod;
			rollingAverage += periods.get(periodIndex++).getPriceAverage();
			rollingAverage -= (ma.size() < maPeriod * 2 - 1) 
							  ? 0 : ma.get(ma.size() - maPeriod);
			rollingAverage /= maPeriod;
			// Add to list of MA
			ma.add(rollingAverage);
		}
		return newLastPeriodNumber;
	}
	
	// Activation stuff
	public boolean isActivated() { return this.activated; }
	// Overload to allow updating of user input upon reactivation if required
	public void reactivate() { this.activated = true; }
	public void reactivate(UserInput userInput) {
		this.activated = true;
		this.userInput = userInput;
	}
	public void deactivate() { this.activated = false; }
	
	
	// Update user input if required
	public void changeUserInput(UserInput userInput) {
		// Update as required
		changeStrategyCapital(userInput.getStrategyCapital());
		changeShortMovingAverage(userInput.getShortPeriod());
		changeLongMovingAverage(userInput.getLongPeriod());
	}
	
	// Change (increase) stratgy capital
	public void changeStrategyCapital(double newAmount) {
		// Only accept increases
		if (newAmount > this.userInput.getStrategyCapital()) {
			// Increase account balance by the difference
			this.userInput.setAccountBalance(this.userInput.getAccountBalance() + 
					(newAmount - this.userInput.getStrategyCapital()));
			// Change strategy capital to new amount
			this.userInput.setStrategyCapital(newAmount);
		}
	}
	
	// Change short moving average
	public void changeShortMovingAverage(int newPeriod) {
		this.lastShortMAPeriodNumber = null;
		this.lastActivityPeriodNumber = null;
		this.shortMAPeriod = newPeriod;
		this.shortMA = new ArrayList<Double>();
	}
	
	// Change long moving average
	public void changeLongMovingAverage(int newPeriod) {
		this.lastLongMAPeriodNumber = null;
		this.lastActivityPeriodNumber = null;
		this.longMAPeriod = newPeriod;
		this.longMA = new ArrayList<Double>();
	}	
	
	// Check if we should permanently exit the strategy
	public void determineForceExit(boolean ignoreProfits) {
		double profitPercentage = StrategyImpl.profitCalculation(this.userInput);
		System.out.print("2MA Profit[Loss] (%): " + profitPercentage + " (");
		System.out.print("AB = " + this.userInput.getAccountBalance());
		System.out.println(",SC = " + this.userInput.getStrategyCapital() + ")");
		// If we made 1% profit or 1% loss
		if (ignoreProfits || profitPercentage >= 1 || profitPercentage <= -1) {
			StrategyImpl.forceExit(this.userInput);
			// Ok can start to kill thread now
			this.waitingForceExit = true;
			// Ask my manager to remove me
			this.twoMAManager.getActivatedStocks().remove(this.userInput.getStockSymbol());			
			// Nothing to buy/sell, exit now
			if (this.userInput.getCurrentStockQuantity() == 0) this.forceExit = true;
		}
	}
	
	public void receiveOrderReply(Order reply) {
		if (reply.getNumFilled() == reply.getQuantityToBuyOrSell()) {
			// This line is needed to make sure this strategy thread has the updated information
			// on the account balance and number of stocks bought etc.
//			this.userInput = reply.getUserInput();
			if (reply.isBuyOrSellFlag()) {
				StrategyImpl.Buy(reply);
			} else {
				StrategyImpl.Sell(reply);
			}
			this.partiallyFilledRetries = 0;
			// If we are waiting for reply after force exit, exit now
			if (this.waitingForceExit) this.forceExit = true;
		} else if (reply.getNumFilled() < reply.getQuantityToBuyOrSell() &&
				   this.partiallyFilledRetries < 3) {
			// update trade message after storing
			Order updatedOrder = StrategyImpl.createOrderRequest(reply.isBuyOrSellFlag(), reply.getUserInput());
			StrategyImpl.SendOrderToMQ(updatedOrder);
			this.partiallyFilledRetries++;
		// Already failed 3 times, just give up and reset counter (no actual activity happend)
		} else {
			this.partiallyFilledRetries = 0;
			this.lastActivityPeriodNumber = null;
			if (this.waitingForceExit) this.forceExit = true;
		}
	}
	
	// Run the strategy
	public void run() {
		// TwoMAStrategy is being activated
		this.activated = true;
		System.out.println("Activated 2MA for " + this.stockSymbol);
		try {
			// Repeat this where required
			while (!this.forceExit) {
				// If we can obtain prices and this strategy is activated
				if (this.stockMonitor.isReady() && this.activated && !this.waitingForceExit) {
					// Ask the stock monitor to monitor the stocks
					this.stockMonitor.updatePrices(this.stockSymbol);
					// Obtain the latest period data
					List<PeriodData> periods = stockMonitor.getStockData(stockSymbol).getPeriods();
					// Update the moving averages
					this.lastShortMAPeriodNumber = updateMovingAverage(periods, this.shortMA, 
						this.lastShortMAPeriodNumber, this.shortMAPeriod, false);
					this.lastLongMAPeriodNumber = updateMovingAverage(periods, this.longMA, 
						this.lastLongMAPeriodNumber, this.longMAPeriod, false);
					// Display last moving average
					System.out.print(this.stockSymbol + " (period: ");
					System.out.print(this.lastShortMAPeriodNumber + ")");
					System.out.print(" | (short MA: ");
					System.out.print(this.shortMA.get(this.shortMA.size() - 1) + ")");
					System.out.print(" | (long MA: ");
					System.out.print(this.longMA.get(this.shortMA.size() - 1) + ")");
					System.out.println();
					// Determine if should send order to buy/sell
					determineBuyOrSell();
					// Determine if we should force exit (via profit)
					determineForceExit(false);
				// If not activated, reset pricing and moving average data
				// (Might be out-dated after a while)
				} else if (!this.activated) {
					this.changeShortMovingAverage(this.shortMAPeriod);
					this.changeLongMovingAverage(this.longMAPeriod);
				}
				// Refresh every 5 seconds
				try {
					Thread.sleep(5000);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		// Other issues
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("2MA ran into an error for " + stockSymbol);
		}
		// Notice of force exit
		System.out.println("forceExit 2MA for " + this.stockSymbol);
	}
}

