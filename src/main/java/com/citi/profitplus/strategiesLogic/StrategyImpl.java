package com.citi.profitplus.strategiesLogic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.citi.profitplus.ProfitplusApplication;
import com.citi.profitplus.entity.Order;
import com.citi.profitplus.entity.OrderRequest;
import com.citi.profitplus.entity.UserInput;

@Service
public class StrategyImpl extends Thread {

	public static void SendOrderToMQ(Order order) {
		// Save to DB to get the id of the order

		
		if (order.getQuantityToBuyOrSell() > 0) {
			Order newOrder = ProfitplusApplication.dbService.insertNewOrder(order);
			try {
				OrderRequest orderRequest = new OrderRequest(newOrder.getOrderId(), newOrder.isBuyOrSellFlag(),
						newOrder.getPrice(), newOrder.getQuantityToBuyOrSell(),
						newOrder.getUserInput().getStockSymbol());
				ProfitplusApplication.jmsService.sendTradeOrder(orderRequest);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		// TODO reply object
	}

	public static void forceExit(UserInput userInput) {
		// userInput =
		// ProfitplusApplication.dbService.findUserInputById(userInput.getUserInputId());
		// if user is still long, sell the remaining stocks
		Order newOrder;
		if (userInput.getCurrentStockQuantity() > 0) {
			newOrder = createOrderRequest(false, userInput);
			if (newOrder.getQuantityToBuyOrSell() > 0 || userInput.getAccountBalance() >= 0) {
				// send mq request
				SendOrderToMQ(newOrder);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}

		// if user is still short, buy back the remaining stocks
		else if (userInput.getCurrentStockQuantity() < 0) {
			newOrder = createOrderRequest(true, userInput);
			if (newOrder.getQuantityToBuyOrSell() > 0 || userInput.getAccountBalance() >= 0) {
				// send mq request
				SendOrderToMQ(newOrder);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}
		userInput.setActivate(false);
		if (ProfitplusApplication.dbService == null)
			return;
		ProfitplusApplication.dbService.updateUserInput(userInput);
		System.out.println("END OF BB");
	}

	/////////////////////////////////////// Calculation//////////////////////////////////////////////

	// Calculate the volume/quantity of stock to buy/sell based on money the
	// strategy is assigned
	public static int maxStockVolume(UserInput userInput, double latestPrice) {
		userInput = ProfitplusApplication.dbService.findUserInputById(userInput.getUserInputId());
		double strategyCapital = userInput.getAccountBalance();
		double buyingPrice = latestPrice;
		int stocksVol = (int) (strategyCapital / buyingPrice);
		if (stocksVol < 0) {
			return 0;
		} else {
			return stocksVol;
		}
	}

	// call with every feed update, ongoing calculation for exit condition
	// Returns the profit/loss relative to original amount
	public static double profitCalculation(UserInput userInput) {
		// userInput =
		// ProfitplusApplication.dbService.findUserInputById(userInput.getUserInputId());
		List<Double> latestFeedPrice = getFeedRestCall(userInput.getStockSymbol());
		double latestFeed = latestFeedPrice.get(latestFeedPrice.size() - 1);

		// Find stock value now v.s. original amount invested
		double stockValue = userInput.getCurrentStockQuantity() * latestFeed;
		double amountInvested = userInput.getCurrentInvestmentAmount();
		
		// No amount invested, no profit to speak of
		if (amountInvested == 0)
			return 0.0;

		System.out.print("Current value = " + stockValue + ",");
		System.out.println("Original value = " + amountInvested);

		// Find percentage of stock value to original investment
		double percentage = (stockValue / Math.abs(amountInvested)) * 100;
		// Return percentage of profit / loss
		return percentage - 100.0;
	}

	/////////////////////////////////////// Order
	/////////////////////////////////////// Sending//////////////////////////////////////////////

	// TODO Review this method
	public void receiveOrderReply(Order reply) {    

			if (reply.getNumFilled() == reply.getQuantityToBuyOrSell()) {
				if (reply.isBuyOrSellFlag()) {
					StrategyImpl.Buy(reply);
					
				} else {
					StrategyImpl.Sell(reply);
				}
			} else if (reply.getNumFilled() < reply.getQuantityToBuyOrSell()) {
				// update trade message after storing
//				Order updatedOrder = createOrderRequest(reply.isBuyOrSellFlag(), reply.getUserInput());
//				SendOrderToMQ(updatedOrder);
//				sendNewOrder.sendTradeOrder(updatedOrder);
//				totalOrderQuantity = newOrder.getSize();
				// recurse to listen for response
//				recursiveOrder(reply, newOrder, totalOrderQuantity, loops - 1);
			}
			System.out.println("Reply:++++++++++++++++++++++++++++++++++++++" + reply);
	}

	public static Order createOrderRequest(boolean buyOrSell, UserInput userInput) {
		// userInput =
		// ProfitplusApplication.dbService.findUserInputById(userInput.getUserInputId());
		List<Double> latestFeedPrice = getFeedRestCall(userInput.getStockSymbol());
		double priceToBuyOrSell = latestFeedPrice.get(latestFeedPrice.size() - 1);
		int quantityToBuy = maxStockVolume(userInput, priceToBuyOrSell);
		String stockSymbol = userInput.getStockSymbol();

		Order order;

		if (buyOrSell == true) {
			order = new Order(userInput, true, priceToBuyOrSell, quantityToBuy, stockSymbol);
		} else {
			order = new Order(userInput, false, priceToBuyOrSell, userInput.getCurrentStockQuantity(), stockSymbol);
		}
		return order;
	}

	/////////////////////////////////////// Transaction and Creation of
	/////////////////////////////////////// Order/////////////////////////////////////////
	// Executed after messageQueueReply to update stocksQuantity and balance,get
	/////////////////////////////////////// order from the reply message
	public static void Buy(Order order) {
		UserInput userInput = order.getUserInput();
		UserInput userInputdb = ProfitplusApplication.dbService.findUserInputById(userInput.getUserInputId());
		System.out.println("Executing buy update");
		// update balance of user
		double updatedBalance = userInput.getAccountBalance() - (order.getNumFilled() * order.getPrice());
		userInput.setAccountBalance(updatedBalance);
		userInputdb.setAccountBalance(updatedBalance);
		
		// update quantity of stock user is holding
		int updatedStockQuantity = userInput.getCurrentStockQuantity() + order.getNumFilled();
		userInput.setCurrentStockQuantity(updatedStockQuantity);
		userInputdb.setCurrentStockQuantity(updatedStockQuantity);

		double CurrentInvestmentAmt = userInput.getCurrentInvestmentAmount() + (order.getNumFilled() * order.getPrice());
		userInput.setCurrentInvestmentAmount(CurrentInvestmentAmt);
		userInputdb.setCurrentInvestmentAmount(CurrentInvestmentAmt);
		
		// store updated userInput, balance and stock quantity
		ProfitplusApplication.dbService.updateUserInput(userInputdb);
	}

	// Executed after messageQueueReply to update stocksQuantity and balance,get
	// order from the reply message
	public static void Sell(Order order) {
		UserInput userInput = order.getUserInput();
		UserInput userInputdb = ProfitplusApplication.dbService.findUserInputById(userInput.getUserInputId());
		// update balance of user
		double updatedBalance = userInput.getAccountBalance() + (order.getNumFilled() * order.getPrice());
		userInput.setAccountBalance(updatedBalance);
		userInputdb.setAccountBalance(updatedBalance);

		// update quantity of stock user is holding
		int updatedStockQuantity = userInput.getCurrentStockQuantity() - order.getNumFilled();
		userInput.setCurrentStockQuantity(updatedStockQuantity);
		userInputdb.setCurrentStockQuantity(updatedStockQuantity);

		// Negative = "sell first (borrowed), buy later"
		double CurrentInvestmentAmt = -1 * (userInput.getCurrentInvestmentAmount()+(order.getNumFilled() * order.getPrice()));
		userInput.setCurrentInvestmentAmount(CurrentInvestmentAmt);
		userInputdb.setCurrentInvestmentAmount(CurrentInvestmentAmt);
		
		// store updated userInput, balance and stock quantity
		ProfitplusApplication.dbService.updateUserInput(userInputdb);
	}

	public static List<Double> getFeedRestCall(String symbol) {
		int maxNumberOfPeriod = 100;
		maxNumberOfPeriod = 20 * maxNumberOfPeriod;
		RestTemplate template = new RestTemplate();
		List<Double> latestPriceFeed = new ArrayList<Double>();

		ParameterizedTypeReference<List> responseType = new ParameterizedTypeReference<List>() {
		};
		ResponseEntity<List> response = template
				.exchange("http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/" + symbol + "?HowManyValues="
						+ maxNumberOfPeriod, HttpMethod.GET, null, responseType);
		List responseListXML = response.getBody();
		for (int i = 0; i < maxNumberOfPeriod; i++) {
			String segments[] = responseListXML.get(i).toString().split(",");
			String price = segments[1];
			String segmentsTwo[] = price.split("=");
			double priceValue = new Double(segmentsTwo[1]);
			latestPriceFeed.add(priceValue);
		}
		return latestPriceFeed;
	}

	public void executeStrategy() {

	}
}
