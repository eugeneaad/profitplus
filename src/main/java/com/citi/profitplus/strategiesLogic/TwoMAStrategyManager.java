package com.citi.profitplus.strategiesLogic;


import java.util.HashMap;
import java.util.Map;

import com.citi.profitplus.ProfitplusApplication;
import com.citi.profitplus.entity.UserInput;

public class TwoMAStrategyManager implements IStrategyManager {
	
	// Use the stock price monitor
	private StockMonitor stockMonitor;
	
	// Keep track of threads for each stock
	Map<String, TwoMAStrategyThread> activatedStocks;
	
	public TwoMAStrategyManager(StockMonitor stockMonitor) {
		this.activatedStocks = new HashMap<String, TwoMAStrategyThread>();
		this.stockMonitor = stockMonitor;
	}
	
	// Activate the strategy for a particular stock with a certain capital amount
	public void activate(UserInput userInput) {
		// Check if it exists first, and if so is it still alive
		if (this.activatedStocks.containsKey(userInput.getStockSymbol()) &&
			this.activatedStocks.get(userInput.getStockSymbol()).isAlive()) {
			System.out.println("Reactivate existing 2MA thread for: " + userInput.getStockSymbol());
			// Then we just reactivate the strategy
			this.activatedStocks.get(userInput.getStockSymbol()).reactivate();
		// Otherwise start the first instance of that strategy
		} else {
			System.out.println("Creating new 2MA thread for: " + userInput.getStockSymbol());
			// Create a stock
			this.activatedStocks.put(userInput.getStockSymbol(), 
				new TwoMAStrategyThread(userInput, this.stockMonitor, this));
			// Start using the strategy
			this.activatedStocks.get(userInput.getStockSymbol()).start();
		}
	}
	
	// Allow for updating of user input
	// Assumption: corresponding stock symbol is already inside user input
	// Note: Only updates strategy capital, short and long MA period
	public void updateUserInput(UserInput userInput) {
		// If we have a strategy for that stock running
		if (this.activatedStocks.containsKey(userInput.getStockSymbol()) &&
				this.activatedStocks.get(userInput.getStockSymbol()).isAlive()) {
			// Update user input data
			this.activatedStocks.get(userInput.getStockSymbol()).changeUserInput(userInput);
		}
		// Ignore otherwise
	}
	
	public Map<String, TwoMAStrategyThread> getActivatedStocks() {
		return this.activatedStocks;
	}
	
	// Deactivate the strategy for a particular stock
	public void deactivate(UserInput userInput) {
		userInput.setActivate(false);
		ProfitplusApplication.dbService.updateUserInput(userInput);
		this.activatedStocks.get(userInput.getStockSymbol()).deactivate();
		
	}
	
	// Force exit a thread
	public void forceExit(UserInput userInput) {
		// Ignore profit constraints, just force the exit...
		this.activatedStocks.get(userInput.getStockSymbol()).determineForceExit(true);
		ProfitplusApplication.dbService.updateUserInput(userInput);
	}
	
}

